# qtpdb

Allows using pdb in PyQt application. To set a breakpoint, simply add the
following line where you want to set a breakpoint.
```
from qtpdb import QtPdb; QtPdb.set_trace()
```
