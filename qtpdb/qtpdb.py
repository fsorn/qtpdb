from typing import Callable, Type, Any


class QtPdb:

    """
    This class allows using pdb's set trace in qt applications without
    warngings being raised. Instead of "pdb.set_trace()", use as
    "QtPdb.set_trace()". QtPdb does not need to be instantiated.
    """

    def __init__(self):
        raise ValueError(QtPdb.__doc__)

    class __readonly_classproperty:
        """Class Property decorator so we do not have to create instances"""

        def __init__(self, f: Callable):
            self.__f = f

        def __get__(self, _: Any, owner: Type):
            return self.__f(owner)

    @__readonly_classproperty
    def set_trace(cls) -> Callable[[], None]:
        """
        Setting a pdb breakpoint does lead to warnings being printed in a qt
        application. To make this properly work, we have to remove input hooks.
        """
        from qtpy.QtCore import pyqtRemoveInputHook
        from pdb import set_trace  # noqa
        pyqtRemoveInputHook()
        return set_trace
